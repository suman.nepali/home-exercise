import express from "express";
import bodyParser from "body-parser";
import cors from "cors";


const app = express();
const port = 3000;
// studnets will be added
let students = [];

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.send("Welcome!");
});


app.post("/student", (req, res) => {
    console.log("POST request init!");
    const data = req.body;
    console.log(data);
    students.push(data);
    res.send("Student's detail is added to the database");
});

app.get("/student/:id", (req, res) => {
    const key = req.params.id;
    console.log(key);

    for(let student of students) {
        console.log(student.id);
        if(student.id == key) {
            res.json(student);
            return;
        }
    }
    res.status(404).send("Student not found");
});

app.delete("/student/:id", (req, res) => {
    const key = req.params.id;

    //remove data from students array
    students = students.filter(i => {
        if(i.id != key) {
            return true;
        }
        return false;
    });
    res.send("Data deleted");
});

app.listen(port, () => {
    console.log(`App is listening on port ${port}`);
});