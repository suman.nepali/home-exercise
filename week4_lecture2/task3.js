
let element = document.getElementById("inputField");
let btnName = document.getElementById("toggleBtn");

// toggle button for input form
// also change the inner value of button
function toggleForm() {
    if(element.style.display === "none") {
        element.style.display = "block";
    } else {
        element.style.display = "none";
    }
}


let postList = document.getElementById("postList");
let submitBtn = document.getElementById("submit-post");
let username = document.getElementById("username");
let userpost = document.getElementById("userpost");

function createPost() {
    let user_name = username.value;
    let user_post = userpost.value;

    if(user_name === "" | user_post === "") {
        return;
    }

    let li = document.createElement("li");

    let name = document.createElement("h2");
    name.classList.add("heading");
    name.textContent = user_name;

    let paragraph = document.createElement("p");
    paragraph.classList.add("paragraph");
    paragraph.textContent = user_post;


    let del = document.createElement("button");
    del.classList.add("delete");
    del.innerHTML = "delete";


    li.appendChild(name);
    li.appendChild(name);
    li.appendChild(name);
    li.appendChild(name);

    username.value = "";
    userpost.value = "";

}

submitBtn.addEventListener("click", createPost);

username.addEventListener("keypress", function(event) {
    if(event.key === "Enter") {
        createPost();
    }
});

