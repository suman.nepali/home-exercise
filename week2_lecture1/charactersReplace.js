// replace every occurance of your given character with given character

let string_to_replace = "Okay lets see the power of replace";

let char_to_replace = process.argv[2];
let char_to_replace_with = process.argv[3];

/** replace will only change the first charcter, whereas
 *  replaceAll will change all the matching characters.
 */
let string_replaced = string_to_replace.replaceAll(char_to_replace, char_to_replace_with);
console.log(string_replaced);