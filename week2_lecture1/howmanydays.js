// return number of days in a month

const d = new Date();
let currentYear = d.getFullYear();

console.log(`This is ${currentYear}`);
let month = process.argv[2];
if(month == 0 || month > 12) {
    console.log("Provided month number is not valid.");
}
else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month ==10 || month ==12) {
    console.log(`The month ${month} has 31 days.`);
}
else if(month == 2 && (currentYear%4==0)) {
    console.log(`This is leap year, month ${month} has 29 days.`);
}
else if(month == 2 && (currentYear%4 !=0)) {
    console.log(`This month ${month} has 28 days.`);
}
else {
    console.log(`The month ${month} has 30 days.`);
}

