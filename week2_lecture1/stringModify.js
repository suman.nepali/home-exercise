// convert the string to lowercase and uppercase;

let str = "Santa Maria";
let strToLowerCase = str.toLowerCase();
let strToUpperCase = str.toUpperCase();
console.log(strToLowerCase);
console.log(strToUpperCase);

// drop off the last word from the string

let string = "Hello Santa Maria";
let lastIndex = string.lastIndexOf(" ");
let final_string = string.substring(0, lastIndex);
console.log(final_string);
