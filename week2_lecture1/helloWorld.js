// return hello world based on input language code

console.log("The available codes are 'fi', 'en', 'es'");

let code = process.argv[2];
if(code == "fi") {
    console.log("Terve!");
}
else if(code == "en") {
    console.log("hello world");
}
else if(code == "es") {
    console.log("Bonjour!");
}
else {
    console.log("Hello world");
}
