// npm init
// set type: module in package.json
// npm install axios --save
// import axios from "axios"

import axios from "axios";

axios.get("https://jsonplaceholder.typicode.com/todos/")
    .then(response => console.log(response.data))
    .catch(error => console.log(error));


// axios.get("https://jsonplaceholder.typicode.com/users/5")
//     .then(response => console.log(response.data))
//     .catch(error => console.log(error));

axios.post("https://jsonplaceholder.typicode.com/users/5", {
    "userId" : "5",
    "id" : "1000"
})
    .then(response => console.log(response.data))
    .catch(error => console.log(error));
