// check exam from two arrays
// +4 for each correct anser, -1 for incorrect and 0 for blank answer
// if the score <0, return 0

let keyArray = ["a", "a", "b", "b"];
let studentAnsArray = ["a", "a", "b", "b"];

let score = 0;

function checkExam(keyArray, studentAnsArray) {
    for(let i = 0; i < keyArray.length; i++) {
        if(keyArray[i] == studentAnsArray[i]) {
            score = score + 4;
        }
        else if(keyArray[i] != studentAnsArray[i]) {
            score = score - 1;
        }   
        else {
            score = score + 0;
        } 
    } 
    return score > 0 ? `The score is ${score}` : "Your score is 0";
}

console.log(checkExam(keyArray, studentAnsArray));



