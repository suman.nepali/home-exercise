// array containing values greater than average

let arr = [2, 4, 6, 1, 7];
let aboveAverageArray = [];

let sumOfElement = arr.reduce(function(a, b) {
    return a + b;
}, 0);
let averageNum = sumOfElement / arr.length;

function aboveAverage(arr) {
    for(let i = 0; i<arr.length; i++) {
        if(arr[i] > averageNum) {
            aboveAverageArray.push(arr[i]);
        }
        
    }
    return aboveAverageArray;
}

console.log(aboveAverage(arr));