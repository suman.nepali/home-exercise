
// calculator

let operator = String(process.argv[2]);
let num1 = Math.floor(process.argv[3]);
let num2 = Math.floor(process.argv[4]);

function calculator(operator, num1, num2) {
    console.log(operator);
    if(operator === "+") {
        return num1 + num2;
    }
    else if(operator === "/") {
        return num1 / num2;
    }
    else if(operator === "*") {
        return num1 * num2;
    }
    else if(operator === "-") {
        if(num1 > num2) {
            return num1 - num2;
        }
        else {
            return num2 - num1;
        }
    }
    else {
        return "Can't do that.";
    }
}

console.log(calculator(operator, num1, num2));