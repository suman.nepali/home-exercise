// count vowels
// a, e , i, o, u, y are vowels in the exercise

let str = "abracadayebra";
let splitedStr = str.split("");

let vowels = ["a", "e", "i", "o", "u", "y"];

function getVowelCount() {
    let count = 0;
    for(let i = 0; i<splitedStr.length; i++) {
        if(vowels.includes(splitedStr[i])) {
            count++;
        }
    }
    return count;
}

console.log(getVowelCount(splitedStr));