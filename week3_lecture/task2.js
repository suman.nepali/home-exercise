// counting likes
// add or reduce the element in arr to check the result.

let arr = ["Maria", "Peter", "Jack"];

function likes(arr) {
    if(arr.length === 0) {
        return "No one likes this";
    }
    else if(arr.length < 4) { 
        return `${arr[0]}, ${arr[1]}, and ${arr[2]} like this`;
    }
    else {
        let more = arr.length - 2;
        return `${arr[0]}, ${arr[1]} and ${more} others`;
    }
}

console.log(likes(arr));