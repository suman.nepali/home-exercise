import { useState } from "react";
import './App.css';

function App() {

  const [result, setResult] = useState("");

  const handleChange = (e) => {
    setResult(result.concat(e.target.name));
  }

  const calculate = () => {
    try {
      setResult(eval(result).toString());
    } catch (err) {
      setResult("Error");
    }
  }
  return (
    <div className="App">
      <div>
      <form>
        <input type="text" value={result}/>
      </form>
      </div>
      <div className="keyboard">
        <button name="7" className="btn btn1" onClick={handleChange}>7</button>
        <button name="8" className="btn btn1" onClick={handleChange}>8</button>
        <button name="9" className="btn btn1" onClick={handleChange}>9</button>
        <button name="/" className="btn btn-3" onClick={handleChange}>/</button>
        <button name="4" className="btn btn1" onClick={handleChange}>4</button>
        <button name="5" className="btn btn1" onClick={handleChange}>5</button>
        <button name="6" className="btn btn1" onClick={handleChange}>6</button>
        <button name="*" className="btn btn-4" onClick={handleChange}>&times;</button>
        <button name="1" className="btn btn1" onClick={handleChange}>1</button>
        <button name="2" className="btn btn1" onClick={handleChange}>2</button>
        <button name="3" className="btn btn1" onClick={handleChange}>3</button>
        <button name="-" className="btn btn5" onClick={handleChange}>-</button>
        <button name="0" className="btn btn1" onClick={handleChange}>0</button>
        <button name="." className="btn btn6" onClick={handleChange}>.</button>
        <button name="+" className="btn btn7" onClick={handleChange}>+</button>
        <button name="=" className="btn btn-9" onClick={calculate}>=</button>
      </div>
    </div>
  );
}

export default App;
