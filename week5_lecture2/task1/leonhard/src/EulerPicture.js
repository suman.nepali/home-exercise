import "./index.css";

function ElurePicture() {
    return(
        <div className="detail">
            <img src="https://upload.wikimedia.org/wikipedia/commons/c/c2/Leonhard_Euler_-_edit1.jpg" alt="Leonhard Euler Image" />
            <p>Leonhard Euler was a Swiss mathematician, physicist, astronomer, geographer, logician and engineer who founded the studies 
                of graph theory and topology and made pioneering and influential discoveries in many other branches of mathematics such as 
                analytic number theory, complex analysis, and infinitesimal calculus. He introduced much of modern mathematical terminology 
                and notation, including the notion of a mathematical function.[3] He is also known for his work in mechanics, fluid dynamics, optics, astronomy and music theory. 
            </p>
        </div>
    );
}

export default ElurePicture;