
import './App.css';

function Singles() {
  const singles = [
    {title: "Du hast", album: "Sehnsucht", year: 1997},
    {title: "Ich will", album: "Mutter", year: 2001},
    {title: "Feuer frei", album: "Mutter", year: 2002},
    {title: "Amerika", album: "Reise, Reise", year: 2004},
    {title: "Mann gegen Mann", album: "Rosenrot", year: 2005},
    {title: "Deutschland", album: "Untitled", year: 2019},
    {title: "Zick Zack", album: "Zeit", year: 2022},
    ]
    const listItems = singles.map((element) => {
      return (
        <ul>
          <li>
            <p><b>Title: {element.title}</b></p>
            <p>Album: {element.album}</p>
            <p>Year: {element.year}</p>
          </li>
        </ul>
      )
    })
    return(
      <div>
        {listItems}
      </div>
    )
}

function App() {
  return (
    <div className="App">
      <Singles /> 
    </div>
  );
}

export default App;
