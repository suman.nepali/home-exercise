import { useState } from "react";
import './App.css';

function HandleChange() {
  const [text, setText] = useState("");
  return(
    <div>
      <h2>{text}</h2>
      <form>
        <label>Input anything here</label>
        <input type="text" onChange={e => setText(e.target.value)} />
        
      </form>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <h2>React input</h2>
      <form>
        <HandleChange />
        
      </form>
    </div>
  );
}

export default App;
