// 1st competitor was Julia,...
const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; 
const ordinals = ['st', 'nd', 'rd', 'th'];
let finalArr = [];

let index = 1;
for(const name of competitors) {
    let ordinal = "";
    if(index === 1) {
        ordinal = ordinals[0];
    }
    else if(index === 2) {
        ordinal = ordinals[1];
    }
    else if(index === 3) {
        ordinal = ordinals [2];
    }
    else {
        ordinal = ordinals[3];
    }
    finalArr.push(`${index}${ordinal} competitor was ${name}`);
    index++;
}

console.log(finalArr);
